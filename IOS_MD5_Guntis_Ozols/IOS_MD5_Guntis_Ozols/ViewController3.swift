//
//  ViewController3.swift
//  IOS_MD5_Guntis_Ozols
//
//  Created by Gman on 24/05/2018.
//  Copyright © 2018 Guntis Ozols. All rights reserved.
//

import UIKit
import MapKit

class ViewController3: UIViewController, UITextFieldDelegate{
    
    var delegate: ViewController3Delegate?
    
    @IBOutlet weak var latText: UITextField!
    @IBOutlet weak var lonText: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        latText.delegate = self
        lonText.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func pievienot(_ sender: Any) {
        if  let latitude = Double(latText.text!), let longitude = Double(lonText.text!){
            let loc = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            
            if CLLocationCoordinate2DIsValid(loc){
                delegate?.addNewMapPoint(coordinates:loc)
            }
            else
            {
                
            }
        }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if (textField.text == "" && string == "-") || (textField.text == "-" && string == ""){
            return true
        }
        
        if textField.text != "" || string != ""{
            let res = (textField.text ?? "") + string
            return Double(res) != nil
        }
        
        return true
    }
        func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
}
protocol ViewController3Delegate: class{
    func addNewMapPoint(coordinates:CLLocationCoordinate2D)
}
